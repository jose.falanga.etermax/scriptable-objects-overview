using UnityEngine;

namespace Usos.Strategies
{
    [CreateAssetMenu(fileName = "Set Sprite", menuName = "Strategies/Set Sprite", order = 0)]
    public class SetSpriteStrategy : SetContentStrategy
    {
        [SerializeField] public Sprite sprite;
        public override void ApplyTo(IDumb dummy)
        {
            dummy.SetSprite(sprite);
        }
    }
}