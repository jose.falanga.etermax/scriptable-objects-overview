using UnityEngine;

namespace Usos.Strategies
{
    [CreateAssetMenu(fileName = "Play Sound", menuName = "Strategies/Play Sound")]
    public class PlaySoundStrategy : SetContentStrategy
    {
        public AudioClip clip;

        public override void ApplyTo(IDumb dummy)
        {
            dummy.PlaySound(clip);
        }
    }
}