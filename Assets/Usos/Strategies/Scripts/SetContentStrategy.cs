using UnityEngine;

namespace Usos.Strategies
{
    public abstract class SetContentStrategy : ScriptableObject
    {
        public abstract void ApplyTo(IDumb dummy);
    }
}