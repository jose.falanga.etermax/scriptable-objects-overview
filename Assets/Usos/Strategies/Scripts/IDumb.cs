using UnityEngine;

namespace Usos.Strategies
{
    public interface IDumb
    {
        void PlaySound(AudioClip audioClip);
        void SetSprite(Sprite sprite);
    }
}