using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Usos.Strategies
{
    public class DumbMono : MonoBehaviour, IDumb
    {
        public List<SetContentStrategy> strategies;

        public AudioSource audioSource;
        public Image image;

        private IEnumerator Start()
        {
            while (strategies.Count > 0)
            {
                yield return new WaitForSeconds(1);
                strategies[0].ApplyTo(this);

                strategies.RemoveAt(0);
            }
        }

        public void PlaySound(AudioClip audioClip)
        {
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        public void SetSprite(Sprite sprite)
        {
            image.sprite = sprite;
        }
    }
}