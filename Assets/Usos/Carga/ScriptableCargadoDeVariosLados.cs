using UnityEngine;

namespace Usos.Carga
{
    [CreateAssetMenu(fileName = "ScriptableCargadoDeVariosLados", menuName = "Carga/ScriptableCargadoDeVariosLados", order = 0)]
    public class ScriptableCargadoDeVariosLados : ScriptableObject
    {
        public int age;
        public new string name;
        public bool mustShow;

        public override string ToString()
        {
            return mustShow ? $"Name: {name} - Age: {age}" : "Hidden Data :3";
        }
    }
}