using System.Collections;
using UnityEditor;
using UnityEngine;

namespace Usos.Carga
{
    public class DataLoader : MonoBehaviour
    {
        public ScriptableCargadoDeVariosLados fromEditor;

        public IEnumerator Start()
        {
            yield return new WaitForSeconds(1);

            Debug.Log("<b>Cargado por Editor</b>: " + fromEditor);

            yield return new WaitForSeconds(1);

            var fromResources = Resources.Load<ScriptableCargadoDeVariosLados>("LaVieja");

            Debug.Log("<b>Cargado por Resources</b>: " + fromResources);

            yield return new WaitForSeconds(1);

            var fromJson = ScriptableObject.CreateInstance<ScriptableCargadoDeVariosLados>();
            JsonUtility.FromJsonOverwrite(Json(),fromJson);

            Debug.Log("<b>Cargado por Json</b>: " + fromJson);

            yield return new WaitForSeconds(1);

            var fromJson2 = ScriptableObject.CreateInstance<ScriptableCargadoDeVariosLados>();
            JsonUtility.FromJsonOverwrite(Json2(),fromJson2);

            Debug.Log("<b>Cargado por Json 2</b>: " + fromJson2);

            // yield return new WaitForSeconds(2);

            // var inMemoryScriptableObject = ScriptableObject.CreateInstance<ScriptableCargadoDeVariosLados>();
            // JsonUtility.FromJsonOverwrite(Json2(),inMemoryScriptableObject);
            // AssetDatabase.CreateAsset(inMemoryScriptableObject,"Assets/scriptableFromCode.asset");

            // Debug.Log("<b>ScriptableObject guardado en disco</b>");
        }

        private string Json() =>
            @"{""age"": 65, ""name"": ""El Chavo del 8"", ""mustShow"": true}";

        private string Json2() =>
            @"{""age"": 90, ""name"": ""Grandpa Shark"", ""mustShow"": false}";
    }
}