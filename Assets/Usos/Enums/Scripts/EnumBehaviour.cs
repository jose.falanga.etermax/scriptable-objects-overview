﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Usos.Enums.Scripts
{
    public enum PowerUp
    {
        Bomb = 1,
        Laser = 2,
        Missile = 3,
        Shield = 4
    }

    [Serializable]
    public class PowerUpItem
    {
        public PowerUp type;
        public string name;
        public Sprite icon;
    }

    public class EnumBehaviour : MonoBehaviour
    {
        public List<PowerUpItem> items;
        public PowerUp selectedPowerUp;

        public TMP_Text nameText;
        public Image iconImage;

        private void Start()
        {
            var selectedItem = items.First(x => x.type == selectedPowerUp);

            if (selectedItem == null) return;

            nameText.text = selectedItem.name;
            iconImage.sprite = selectedItem.icon;
        }
    }
}