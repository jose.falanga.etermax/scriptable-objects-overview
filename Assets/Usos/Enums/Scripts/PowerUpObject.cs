using UnityEngine;

namespace Usos.Enums.Scripts
{
    [CreateAssetMenu(fileName = "PowerUp", menuName = "Inventory/PowerUp", order = 0)]
    public class PowerUpObject : ScriptableObject
    {
        public string name;
        public Sprite icon;
    }
}