﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Usos.Enums.Scripts
{
    public class ScriptableEnumBehaviour : MonoBehaviour
    {
        public PowerUpObject powerUpObject;

        public TMP_Text nameText;
        public Image iconImage;

        private void Start()
        {
            if (powerUpObject == null) return;

            nameText.text = powerUpObject.name;
            iconImage.sprite = powerUpObject.icon;
        }
    }
}
