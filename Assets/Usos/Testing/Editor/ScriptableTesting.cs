using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using Usos.Carga;
using Usos.Strategies;

namespace Usos.Testing.Editor
{
    /*
     * PowerUpObject, ConfigurationScriptable: no tienen logica, no tiene sentido testear
     * DynamicAudio se usa para manipular un AudioSource, no se puede testear que reciba llamadas con un mock
     * AssetMap se podria testear, pero no veo que aporte mucho valor como ejemplo
     */

    public class ScriptableTesting
    {
        private static AudioClip EmptyClip =>
            AudioClip.Create("sarasa",1,1,1000,false);

        private static Sprite EmptySprite =>
            Sprite.Create(Texture2D.whiteTexture, new Rect(), Vector2.one);

        [Test]
        public void test_printing_shown()
        {
            var toPrint = ScriptableObject.CreateInstance<ScriptableCargadoDeVariosLados>();
            toPrint.age = 99;
            toPrint.name = "Juanito";
            toPrint.mustShow = true;
            var expectedPrint = "Name: Juanito - Age: 99";

            var printed = toPrint.ToString();

            Assert.AreEqual(expectedPrint, printed);
        }

        [Test]
        public void test_printing_hidden()
        {
            var toPrint = ScriptableObject.CreateInstance<ScriptableCargadoDeVariosLados>();
            toPrint.age = 10;
            toPrint.name = "Beto";
            toPrint.mustShow = false;
            var expectedPrint = "Hidden Data :3";

            var printed = toPrint.ToString();

            Assert.AreEqual(expectedPrint, printed);
        }

        [Test]
        public void test_sound_strategy()
        {
            var dumbo = Substitute.For<IDumb>();
            var strategy = ScriptableObject.CreateInstance<PlaySoundStrategy>();
            strategy.clip = EmptyClip;

            strategy.ApplyTo(dumbo);

            dumbo.Received(1).PlaySound(Arg.Is<AudioClip>(a => a.name == EmptyClip.name));
        }

        [Test]
        public void test_sprite_strategy()
        {
            var dumbo = Substitute.For<IDumb>();
            var strategy = ScriptableObject.CreateInstance<SetSpriteStrategy>();
            strategy.sprite = EmptySprite;

            strategy.ApplyTo(dumbo);

            dumbo.Received(1).SetSprite(Arg.Is<Sprite>(s => s.texture == EmptySprite.texture));
        }
    }
}