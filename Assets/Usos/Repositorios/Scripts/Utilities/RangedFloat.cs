﻿using System;

namespace Usos.Repositorios.Scripts.Utilities
{
	[Serializable]
	public struct RangedFloat
	{
		public float minValue;
		public float maxValue;
	}
}