using DG.Tweening;
using UnityEngine;

namespace Usos.Repositorios.Scripts.Scriptables
{
    [CreateAssetMenu(fileName = "AnimationConfiguration", menuName = "Repositories/Configuration")]
    public class ConfigurationScriptable : ScriptableObject
    {
        [Tooltip("-1 for infinite loops")]
        public int loops;

        public Ease ease = Ease.Linear;

        [Range(.1f,10f)]
        public float duration;

        private void OnValidate()
        {
            loops = (int) Mathf.Clamp(loops, -1, Mathf.Infinity);
        }
    }
}