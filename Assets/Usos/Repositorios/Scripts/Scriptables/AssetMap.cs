using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Usos.Repositorios.Scripts.Scriptables
{
    [CreateAssetMenu(fileName = "AssetMap", menuName = "Repositories/AssetMap")]
    public class AssetMap : ScriptableObject
    {
        [SerializeField] private List<Item> items;

        public Item GetItem(string name)
        {
            return items.First(item => item.name == name);
        }

        public Item GetRandomItem()
        {
            return items[Random.Range(0, items.Count)];
        }
    }

    [Serializable]
    public class Item
    {
        public string name;
        public Sprite sprite;
    }
}