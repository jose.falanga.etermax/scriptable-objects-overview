﻿using UnityEngine;
using Usos.Repositorios.Scripts.Utilities;
using Random = UnityEngine.Random;

namespace Usos.Repositorios.Scripts.Scriptables
{
	[CreateAssetMenu(fileName = "DynamicAudio", menuName="Repositories/DynamicAudio")]
	public class DynamicAudio : ScriptableObject
	{
		public AudioClip[] clips;

		public RangedFloat volume;

		[MinMaxRange(0, 2)]
		public RangedFloat pitch;

		public void Play(AudioSource source)
		{
			if (clips.Length == 0) return;

			source.clip = clips[Random.Range(0, clips.Length)];
			source.volume = Random.Range(volume.minValue, volume.maxValue);
			source.pitch = Random.Range(pitch.minValue, pitch.maxValue);
			source.Play();
		}
	}
}