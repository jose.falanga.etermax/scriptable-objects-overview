﻿using UnityEngine;
using UnityEngine.UI;
using Usos.Repositorios.Scripts.Scriptables;

namespace Usos.Repositorios.Scripts.View
{
    [RequireComponent(typeof(Button))]
    public class AudioButton : MonoBehaviour
    {
        private AudioSource source;
        public DynamicAudio audioProvider;

        void Start()
        {
            source = gameObject.AddComponent<AudioSource>();
            GetComponent<Button>().onClick.AddListener(PlayAudio);
        }

        private void PlayAudio()
        {
            audioProvider.Play(source);
        }
    }
}
