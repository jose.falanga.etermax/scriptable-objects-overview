using DG.Tweening;
using UnityEngine;
using Usos.Repositorios.Scripts.Scriptables;

namespace Usos.Repositorios.Scripts.View
{
    public class TweenedItem : SomeItem
    {
        public ConfigurationScriptable config;
        private void Start()
        {
            DoCoolLoop();
        }

        void DoCoolLoop()
        {
            DoRotateImage()
                .SetLoops(config.loops);

            DOTween.Sequence()
                .Append(DoItemScale(0)).Join(DoTextAlpha(0))
                .AppendCallback(SetDataFromRandomItem)
                .Append(DoItemScale(1)).Join(DoTextAlpha(1))
                .AppendInterval(config.duration)
                .SetLoops(config.loops);
        }

        private void SetDataFromRandomItem()
        {
            SetDataFromItem(assetAssetMap.GetRandomItem());
        }

        Tweener DoTextAlpha(float value)
        {
            return text.DOFade(value,config.duration/3);
        }

        Tweener DoItemScale(float size)
        {
            return image.transform.DOScale(Vector3.one * size, config.duration/2);
        }

        Tweener DoRotateImage()
        {
            return image
                .transform
                .DORotate(Vector3.back * 360, config.duration, RotateMode.FastBeyond360)
                .SetEase(config.ease)
                .SetRelative();
        }
    }
}