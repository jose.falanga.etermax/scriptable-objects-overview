﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Usos.Repositorios.Scripts.Scriptables;

namespace Usos.Repositorios.Scripts.View
{
    public class SomeItem : MonoBehaviour
    {
        public AssetMap assetAssetMap;
        public Image image;
        public TMP_Text text;
        public string itemName;

        private IEnumerator Start()
        {
            while (true)
            {
                if (!string.IsNullOrEmpty(itemName))
                    SetDataFromItem(assetAssetMap.GetItem(itemName));
                yield return new WaitForSeconds(1);
            }
            // ReSharper disable once IteratorNeverReturns
        }
        protected void SetDataFromItem(Item item)
        {
            text.text = item.name;
            image.sprite = item.sprite;
        }
    }
}
