# Scriptable Objects Overview

## Que son los Scriptable Objects?
Objetos serializados persistentes, que pueden contar con su propia logica. ScriptableAssets podria ser un mejor nombre.

## Usos

- [x] Como repositorios locales:
  - [x] **Almacen de datos y configuraciones:** muchos plugins externos los usan para guardar configuraciones como app ids, api tokens. el sistema de addresables los usa fuertemente
  - [x] **Mapeos de IDs a Assets:** como mega diccionarios de referencias locales.
  - [x] **Mapeo procedural:** Una combinacion de los dos anteriores, un repositorio de refrencias al que se le puede configurar variabilidad para que devuelva versiones diferentes de lo que se le pide, como por ejemplo variacion de sonidos en pich, volumen, eco randomizado, partiendo de pocos solo.

- [x] Como reemplazo de enums :
  - [x] Enriquecidos con datos (TIDs, Sprites, como mapeos dinamicos)
  - [x] Extensibles sin necesidad de tocar codigo (artist friendly)

- [x] Como colaboradores inteligentes (principalmente para las vistas)
  - [x] Se les puede delegar logica de vista que depende de configuracion/tuneo de parametros.
  - [x] Las vistas pueden reutilizarlos aunque no tengan nada que ver entre si.
  - [x] Se les puede delegar logica de vista reutilizable, como controllers o strategies, quitandole a la vista responsabilidades.  
  - [x] Se pueden referenciar por editor, cargar desde archivo, crear en runtime, y deserializar desde json.

### Ventajas

- Son facilmente testeables (siempre que no involucren componentes de Unity, por conocidas razones).
- Les otorga mas poder a los artistas y gente no tecnica, sobre cosas que normalmente no tiene.
- Viven practicamente fuera del ciclo de vida de Unity, son casi inertes y no necesitan cuidados especiales.
- Se pueden utilizar fuera de los MonoBehaviours, como dependencias normales y colaboradores externos de cualquier clase (como Lokalise).  
- Si estan serializados en disco, se pueden editar en modo Play, y conservan la configuracion, como los Materials.

### Desventajas

- En la mayoria de los casos, estan atados a cada release de la app.
- Como todo en Unity Editor, es propenso a errores humanos, el famoso dedazo.
- Se pueden volver confusos si se utilizan desordenadamente o con abstracciones intermedias innecesarias.
- Si no se hace un Save Project, las serializaciones a disco no ocurren hasta no cerrar el editor.

## Referencias
- [Slides](https://docs.google.com/presentation/d/1kzfxPZfeMB7EmX_3i44TfYZQs-oUWGjWhK4-AoPVCRc/edit?usp=sharing)
- [Unite 2016 - Overthrowing the MonoBehaviour Tyranny in a Glorious Scriptable Object Revolution](https://www.youtube.com/watch?v=6vmRwLYWNRo) - [Repo](https://github.com/antfarmar/Unity-3D-ScriptableObjects-Tanks)
- [Unite Austin 2017 - Game Architecture with Scriptable Objects](https://www.youtube.com/watch?v=raQ3iHhE_Kk)
- [Overview by Kiwi Argo](https://www.youtube.com/watch?v=0GAkXd_q4os)
- [Unity Atoms](https://medium.com/@adamramberg/unity-atoms-tiny-modular-pieces-utilizing-the-power-of-scriptable-objects-e8add1b95201)
